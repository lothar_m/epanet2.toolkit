# epanet programmer's toolkit

EPANET TOOLKIT by EPA.
This optimized linux version has been adapted by Manuel López-Ibáñes. Please refer to http://iridia.ulb.ac.be/~manuel/epanetlinux for more information.


## From the repository owner:
Please notice that this is a library intended to be used as a interface between epanet2 and third party software. Once installed in the machine, the library and header files must be linked when compiling such 3rd party software.

Read the included documentation for more info.